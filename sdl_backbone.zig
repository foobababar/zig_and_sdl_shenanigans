/// sdl backbone
/// uses array of Pixel on a texture to represent the screen
/// credits to https://lazyfoo.net/tutorials/SDL/01_hello_SDL/index2.php for SDL_Surface stuff

/// build with zig build-exe sdl_hw_3.zig -lc -lSDL2


const std = @import("std");
const print = std.debug.print;
const assert = std.debug.assert;

const sdl = @cImport ({
    @cInclude("SDL2/SDL.h");
});

const Pixel = u32;


pub fn main() !void {
    const WIDTH = 600;
    const HEIGHT = 400;
    const WHITE = 0xFFFFFFFF;
    const RED = 0xFFFF0000;

    var init_status: i32 = sdl.SDL_Init(sdl.SDL_INIT_VIDEO);
    if (init_status != 0) {
        std.os.exit(1);
    }

    var window: *sdl.SDL_Window = sdl.SDL_CreateWindow("sdl app", 0, 0, WIDTH, HEIGHT, sdl.SDL_WINDOW_SHOWN | sdl.SDL_WINDOW_RESIZABLE).?; 

    var renderer: *sdl.SDL_Renderer = sdl.SDL_CreateRenderer(window, -1, sdl.SDL_RENDERER_ACCELERATED).?;
    _ = sdl.SDL_SetRenderDrawBlendMode(renderer, sdl.SDL_BLENDMODE_BLEND);

    var texture: *sdl.SDL_Texture = sdl.SDL_CreateTexture(renderer, sdl.SDL_PIXELFORMAT_ARGB8888, sdl.SDL_TEXTUREACCESS_STREAMING, WIDTH, HEIGHT).? ;
    _ = sdl.SDL_SetTextureBlendMode(texture, sdl.SDL_BLENDMODE_BLEND); // <- enable transparency (2/2)

    var canvas: [WIDTH*HEIGHT]Pixel = undefined;

    for(canvas, 0..) |_, i| {
        if (i<WIDTH*HEIGHT/2) { 
            canvas[i] = WHITE;
        }
        else {
            canvas[i] = RED; 
        }
    }

    assert(canvas[0] == WHITE);
    assert(canvas[WIDTH*HEIGHT/2] == RED);



    var quit: bool = false;


    while (!quit) {
        var event: sdl.SDL_Event = undefined; 
        while (sdl.SDL_PollEvent(&event) != 0) {
            switch (event.type) {
                sdl.SDL_QUIT => {
                    quit = true; 
                },

                sdl.SDL_KEYDOWN => {
                    switch (event.key.keysym.scancode){
                        sdl.SDL_SCANCODE_F => {
                            print("f key pressed \n", .{}); 
                        },
                        sdl.SDL_SCANCODE_A, sdl.SDL_SCANCODE_ESCAPE => {
                            quit = true; 
                        },

                        sdl.SDL_SCANCODE_RIGHT => {
                            print("right arrow pressed \n", .{}); 
                        },
                        else => {}, 
                    }
                },

                sdl.SDL_KEYUP => {
                    switch (event.key.keysym.scancode){
                        sdl.SDL_SCANCODE_F => {
                            print("f key released \n", .{}); 
                        },
                        else => {},
                    }
                },

                sdl.SDL_MOUSEWHEEL => {
                    if (event.wheel.y > 0) {
                        print("mouse scrolling up \n", .{}); 
                    }
                    else if (event.wheel.y < 0) {
                        print("mouse scrolling down \n", .{}); 
                    }
                },

                sdl.SDL_MOUSEBUTTONDOWN => {
                    if (event.button.button == sdl.SDL_BUTTON_LEFT) {
                        print("left click down \n", .{}); 
                    }                 
                    else if (event.button.button == sdl.SDL_BUTTON_RIGHT) {
                        print("right click down\n", .{}); 
                    }
                },

                sdl.SDL_MOUSEBUTTONUP => {
                    if (event.button.button == sdl.SDL_BUTTON_LEFT) {
                        print("left click up \n", .{}); 
                    }                 
                    else if (event.button.button == sdl.SDL_BUTTON_RIGHT) {
                        print("right click up \n", .{}); 
                    }
                },

                sdl.SDL_MOUSEMOTION => {
                    // print("mouse moving \n", .{}); 
                },

                else => {},
            }        
        }

        // here, main loop body
        _ = sdl.SDL_UpdateTexture(texture, null, @ptrCast([*c]const Pixel, canvas[0..]), WIDTH * @sizeOf(Pixel));
        _ = sdl.SDL_RenderClear(renderer);  
        _ = sdl.SDL_RenderCopy(renderer, texture, null, null); 
        _ = sdl.SDL_RenderPresent(renderer); 

    }

    sdl.SDL_DestroyTexture(texture);
    sdl.SDL_DestroyRenderer(renderer);
    sdl.SDL_DestroyWindow(window);
    sdl.SDL_Quit();
}


