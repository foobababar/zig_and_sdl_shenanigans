/// sdl backbone
///
/// credits to https://lazyfoo.net/tutorials/SDL/01_hello_SDL/index2.php for SDL_Surface stuff

/// build with zig build-exe sdl_hw_3.zig -lc -lSDL2

/// TODO : demonstrate SDL_Texture usage
/// TODO : demonustrate loading a bitmap



const std = @import("std");
const print = std.debug.print;

const sdl = @cImport ({
    @cInclude("SDL2/SDL.h");
});


pub fn main() !void {

    var init_status: i32 = sdl.SDL_Init(sdl.SDL_INIT_VIDEO);
    if (init_status != 0) {
        std.os.exit(1);
    }

    var window: *sdl.SDL_Window = sdl.SDL_CreateWindow("sdl app", 0, 0, 600, 400, sdl.SDL_WINDOW_SHOWN | sdl.SDL_WINDOW_RESIZABLE).?; 

    var surface: *sdl.SDL_Surface = sdl.SDL_GetWindowSurface(window);
    _ = sdl.SDL_FillRect( surface, null, sdl.SDL_MapRGB( surface.*.format, 0xFF, 0xFF, 0xFF ) );

    var quit: bool = false;


    while (!quit) {
        var event: sdl.SDL_Event = undefined; 
        while (sdl.SDL_PollEvent(&event) != 0) {
            switch (event.type) {
                sdl.SDL_QUIT => {
                    quit = true; 
                },

                sdl.SDL_KEYDOWN => {
                    switch (event.key.keysym.scancode){
                        sdl.SDL_SCANCODE_F => {
                            print("f key pressed \n", .{}); 
                        },
                        sdl.SDL_SCANCODE_A, sdl.SDL_SCANCODE_ESCAPE => {
                            quit = true; 
                        },

                        sdl.SDL_SCANCODE_RIGHT => {
                            print("right arrow pressed \n", .{}); 
                        },
                        else => {}, 
                    }
                },

                sdl.SDL_KEYUP => {
                    switch (event.key.keysym.scancode){
                        sdl.SDL_SCANCODE_F => {
                            print("f key released \n", .{}); 
                        },
                        else => {},
                    }
                },

                sdl.SDL_MOUSEWHEEL => {
                    if (event.wheel.y > 0) {
                        print("mouse scrolling up \n", .{}); 
                    }
                    else if (event.wheel.y < 0) {
                        print("mouse scrolling down \n", .{}); 
                    }
                },

                sdl.SDL_MOUSEBUTTONDOWN => {
                    if (event.button.button == sdl.SDL_BUTTON_LEFT) {
                        print("left click down \n", .{}); 
                    }                 
                    else if (event.button.button == sdl.SDL_BUTTON_RIGHT) {
                        print("right click down\n", .{}); 
                    }
                },

                sdl.SDL_MOUSEBUTTONUP => {
                    if (event.button.button == sdl.SDL_BUTTON_LEFT) {
                        print("left click up \n", .{}); 
                    }                 
                    else if (event.button.button == sdl.SDL_BUTTON_RIGHT) {
                        print("right click up \n", .{}); 
                    }
                },

                sdl.SDL_MOUSEMOTION => {
                    // print("mouse moving \n", .{}); 
                },

                else => {},
            }        
        }
        _ = sdl.SDL_UpdateWindowSurface(window);

    }

    sdl.SDL_DestroyWindow(window);
    sdl.SDL_Quit();
}
