// example from https://en.wikibooks.org/wiki/SDL_(Simple_DirectMedia_Layer)/Basics/Creating_a_window

//compile with `zig build-exe main.zig -lc -lSDL2`


const std = @import("std");
const print = std.debug.print;

const sdl = @cImport({
  @cInclude("SDL2/SDL.h");
});


pub fn main() !void {
  print("sdl hello world \n", .{});

  var init_status: i64 = sdl.SDL_Init(sdl.SDL_INIT_VIDEO);

  if(init_status != 0){
    print("Failed to initialize sdl\n", .{});
    std.os.exit(1);
  }

  var window: ?*sdl.SDL_Window = sdl.SDL_CreateWindow("SDL hello world !", sdl.SDL_WINDOWPOS_UNDEFINED, sdl.SDL_WINDOWPOS_UNDEFINED, 800, 600, 0);

  if(window == null){
    print("Failed to initialize sdl window\n", .{});
    std.os.exit(1);
  }

  var renderer: *sdl.SDL_Renderer = sdl.SDL_CreateRenderer(window, -1, sdl.SDL_RENDERER_PRESENTVSYNC).?;

  var quit : bool = false;
  var event: sdl.SDL_Event = undefined;

  while(!quit){
    _ = sdl.SDL_WaitEvent(&event); 
    switch(event.type){
      sdl.SDL_QUIT => { //quit when closing window
          quit = true;
          break;
      },
      else => {},
    }
    _ = sdl.SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    _ = sdl.SDL_RenderClear(renderer);
    _ = sdl.SDL_RenderPresent(renderer);
  }

  sdl.SDL_DestroyWindow(window);
  sdl.SDL_Quit();
}

