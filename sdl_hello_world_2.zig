// example from https://en.wikibooks.org/wiki/SDL_(Simple_DirectMedia_Layer)/Basics/Creating_a_window
// resources : 
// https://gigi.nullneuron.net/gigilabs/sdl2-pixel-drawing/
// https://bitbucket.org/dandago/gigilabs/src/master/Sdl2PixelDrawing/Sdl2PixelDrawing/main.cpp


//compile with `zig build-exe main.zig -lc -lSDL2`


const std = @import("std");
const print = std.debug.print;

const sdl = @cImport({
  @cInclude("SDL2/SDL.h");
});


const WIDTH = 600;
const HEIGHT = 480;

pub fn main() !void {

  var init_status: i64 = sdl.SDL_Init(sdl.SDL_INIT_VIDEO);

  if(init_status != 0){
    print("Failed to initialize sdl\n", .{});
    std.os.exit(1);
  }

  var window: ?*sdl.SDL_Window = sdl.SDL_CreateWindow("SDL hello world !", sdl.SDL_WINDOWPOS_UNDEFINED, sdl.SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, 0);

  if(window == null){
    print("Failed to initialize sdl window\n", .{});
    std.os.exit(1);
  }

  var renderer: *sdl.SDL_Renderer = sdl.SDL_CreateRenderer(window, -1, 0).?;
  var texture: *sdl.SDL_Texture = sdl.SDL_CreateTexture(renderer, sdl.SDL_PIXELFORMAT_ARGB8888, sdl.SDL_TEXTUREACCESS_STREAMING, WIDTH, HEIGHT).?; 

  var pixels:[WIDTH*HEIGHT]u32 = undefined; 

  //set all pixels to white
  var i: u32 = 0;
  while(i<pixels.len) : (i+=1){
    pixels[i]=0xFFFFFFFF;
  }
  
  var quit : bool = false;
  var click: bool = false;

  var event: sdl.SDL_Event = undefined;

  while(!quit){

    _ = sdl.SDL_UpdateTexture(texture, null, &pixels, WIDTH*@sizeOf(u32)); //copy pixels into the window


    _ = sdl.SDL_WaitEvent(&event);
    switch(event.type){
      sdl.SDL_QUIT => { //quit when closing window
          quit = true;
      },
      sdl.SDL_KEYDOWN => {
        switch(event.key.keysym.scancode){
          sdl.SDL_SCANCODE_UP => { print("up\n", .{}); },
          sdl.SDL_SCANCODE_DOWN => { print("down\n", .{}); },
          sdl.SDL_SCANCODE_LEFT=> { print("left\n", .{}); },
          sdl.SDL_SCANCODE_RIGHT => { print("right\n", .{}); },
          else => {},
        }
      },
      sdl.SDL_KEYUP => {},


      sdl.SDL_MOUSEBUTTONUP => {
        if(event.button.button == sdl.SDL_BUTTON_LEFT){
          click = false;
        }
      },


      sdl.SDL_MOUSEBUTTONDOWN => {
        if(event.button.button == sdl.SDL_BUTTON_LEFT){
          click = true;
        }
      },

      sdl.SDL_MOUSEMOTION => {
        if(click) { 
          //pixels[@intCast(usize, event.motion.y * WIDTH + event.motion.x-1)] = 0;
          pixels[@intCast(usize, event.motion.y * WIDTH + event.motion.x)] = 0;
          //pixels[@intCast(usize, event.motion.y * WIDTH + event.motion.x+1)] = 0;
        }
      },

      else => {},
    }

    //render the texture
    _ = sdl.SDL_RenderClear(renderer);
    _ = sdl.SDL_RenderCopy(renderer, texture, null, null);
    _ = sdl.SDL_RenderPresent(renderer);
  }

  sdl.SDL_DestroyRenderer(renderer);
  sdl.SDL_DestroyWindow(window);
  sdl.SDL_Quit();
}

